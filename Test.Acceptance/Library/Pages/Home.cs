﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace Test.Acceptance.Library.Pages
{
    public class Home : BasePageModel
    {
        By Alert = By.Id("alert");
        By NewBrowserWindow = By.Id("button1");
        By NewMessageWindow = By.XPath(".//*[@id='content']/div[2]//button[contains(text(),'New Message')]");
        By NewBrowserTab = By.XPath(".//*[@id='content']/div[2]//button[contains(text(),'Browser Tab')]");
        By TimingAlert = By.Id("timingAlert");

        TimeSpan TimeSec = TimeSpan.FromSeconds(1000);

        public void ClickNewBrowserWindow()
        {
            Click(NewBrowserWindow);
        }

        public void ClickNewMessageWindow()
        {
            Click(NewMessageWindow);
        }

        public void ClickNewBrowserTab()
        {
            Click(NewBrowserTab);
        }

        public void ClickJavaScriptAlert()
        {
            Click(Alert);
        }

        public void ValidateAlert()
        {
            // wait for alert to be displayed
            WaitForAlert(TimeSec);

            // switch to the alert
            var alert = driver.SwitchTo().Alert();

            // setting popup properties
            var excpected = "Please share this website with your friends and in your organization.";
            var actual = alert.Text;

            // assert
            Assert.AreEqual(excpected, actual);
        }

        public void ClickAlertConfirmation()
        {
            // switch to the alert
            var alert = driver.SwitchTo().Alert();
            alert.Accept();
            SwitchToParentWindow();
        }

        public void ClickTimingAlert()
        {
            Click(TimingAlert);
        }
    }
}
