﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class NewMessage : BasePageModel
    {
        By NewMessagePopup = By.TagName("body");

        TimeSpan TimeSec = TimeSpan.FromSeconds(120);

        public void ValidateNewMessageWindow()
        {
            var expected = "This message window is only for viewing purposes";
            var actual = driver.FindElement(NewMessagePopup).Text;

            // switch to the message popup window
            SwitchToNewWindow();
            //Assert.AreEqual(expected, actual);

            // Close the message popup and switch to home page
            driver.Close();
            SwitchToParentWindow();
        }
    }
}
