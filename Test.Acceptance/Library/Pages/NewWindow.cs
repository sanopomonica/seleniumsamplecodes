﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class NewWindow : BasePageModel
    {
        //By NewWindowPage = By.XPath(".//*[@id='content']/div[1]/div/div/div/div/h2/span/strong");
        By NewWindowPage = By.XPath("//*[@id='content']//strong[contains(text(),'Agile Testing')]");        
        By Cucumber = By.CssSelector("#dl-menu .menu-item-1521");
        By CucumberJVM = By.CssSelector("#dl-menu .menu-item-1781");
        By CucumberJVMPage = By.CssSelector("#text-10 .widget-title");
        By Menu = By.Id("mobile-menu");

        TimeSpan TimeSec = TimeSpan.FromSeconds(120);

        public void ValidateNewWindow()
        {
            string expected = "Agile Testing and ATDD Automation –  Free Tutorials";

            SwitchToNewWindow();
            Validation(NewWindowPage, expected);
        }

        public void GoToCucumber()
        {
            Click(Menu);
            Click(Cucumber);
            WaitForElement(CucumberJVM, TimeSec);
            Click(CucumberJVM);
        }

        public void ValidateCucumberJVMPage()
        {
            string expected = "CUCUMBER-JVM & SELENIUM – BASIC";

            WaitForElement(CucumberJVMPage, TimeSec);
            Validation(CucumberJVMPage, expected);
            driver.Close();
            SwitchToParentWindow();
        }

    }
}
