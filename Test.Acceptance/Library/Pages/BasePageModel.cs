﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Test.Acceptance.Infrastracture;

namespace Test.Acceptance.Library.Pages
{
    public class BasePageModel
    {
        protected IWebDriver driver = StaticDriver.driver;

        public void Click(By element)
        {
            driver.FindElement(element).Click();
        }

        public void Dispose()
        {
            //driver.Quit();
            //driver = null;
            StaticDriver.CloseDriver();
            Console.WriteLine("Completed Test");
        }

        protected IWebElement FindElement(By element)
        {
            return StaticDriver.driver.FindElement(element);
        }

        public void GoToURL(string URL)
        {
            driver.Url = URL;
            //driver = null;
        }

        public void PopupWindowFinder()
        {
            //PopupWindowFinder finder = new PopupWindowFinder(driver);
            //finder.;
            //driver.
            //driver.SwitchTo().Window(newWin);
            //string popupWindowHandle = finder.Click(FindElement(element));
            //driver.SwitchTo().Window(popupWindowHandle);

            ReadOnlyCollection<string> handles = driver.WindowHandles;
            foreach(var handle in handles)
            {
                Console.WriteLine(handle);
            }
            //driver.SwitchTo().Window(ha);
        }

        public void SwitchToNewWindow()
        {
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }

        public void SwitchToParentWindow()
        {
            driver.SwitchTo().Window(driver.WindowHandles.First());
        }

        public void ValidateActiveWindows()
        {
            var windowList = driver.WindowHandles.ToList();
        }

        public void Validation(By elementActual, string valueExpected)
        {
            IWebElement actual = FindElement(elementActual);
            Assert.AreEqual(valueExpected, actual.Text, "Error: Expected and Actual are not equal");
        }

        public void WaitForAlert(TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.Until(ExpectedConditions.AlertIsPresent());
        }

        public void WaitForElement(By element, TimeSpan timeout)
        {
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }

        public static string SwitchWindow(IWebDriver driver, int timer = 2000)
        {
                        System.Collections.Generic.List<string> previousHandles = new
            System.Collections.Generic.List<string>();
                        System.Collections.Generic.List<string> currentHandles = new
            System.Collections.Generic.List<string>();
                        previousHandles.AddRange(driver.WindowHandles);

            Thread.Sleep(timer);
            for (int i = 0; i < 20; i++)
            {
                currentHandles.Clear();
                currentHandles.AddRange(driver.WindowHandles);
                foreach (string s in previousHandles)
                {
                    currentHandles.RemoveAll(p => p == s);
                }
                if (currentHandles.Count == 1)
                {
                    driver.SwitchTo().Window(currentHandles[0]);
                    Thread.Sleep(100);
                    return currentHandles[0];
                }
                else
                {
                    Thread.Sleep(500);
                }
            }
            return null;
        }
    }
}
