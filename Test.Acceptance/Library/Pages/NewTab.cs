﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Acceptance.Library.Pages
{
    public class NewTab : BasePageModel
    {
        public void ValidateTabTitle()
        {
            string expectedTabTitle = "Selenium Framework | Selenium, Cucumber, Ruby, Java et al.";

            // switch to the new tab and validate the title
            SwitchToNewWindow();
            Assert.AreEqual(expectedTabTitle, driver.Title);

            // Close the tab then switch to the pervious window
            driver.Close();
            SwitchToParentWindow();
        }
    }
}
