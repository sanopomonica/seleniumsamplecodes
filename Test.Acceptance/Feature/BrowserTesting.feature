﻿Feature: BrowserTesting

Scenario: Test the New Browser Window
	Given I go to Selenium Framework website
	When I click the new browser window button
	Then I will see the new browser popup will open
	When I go to cucumber tutorial
	Then I be directed in Cucumber Tutorial page


Scenario: Test the New Message Window
	Given I go to Selenium Framework website
	When I click the new message browser button
	Then I will see the new message popup is displayed

Scenario: Test the New Browser Tab Window
	Given I go to Selenium Framework website
	When I click the new browser tab button
	Then I will see the new browser tab is displayed

Scenario: Test the Alert Box
	Given I go to Selenium Framework website
	When I click the alert button
	Then I will see the popup message
	And I can click ok in the alert popup

Scenario: Test the Timing Alert Box
	Given I go to Selenium Framework website
	When I click the timing alert button
	Then I will see the popup message

