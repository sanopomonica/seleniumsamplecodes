﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using TechTalk.SpecFlow;

namespace Test.Acceptance.StepDefinition
{
    [Binding]
    class DataCollectionStepsDefinition : BaseStepDefinition
    {
        [Given(@"I go to Selenium Framework website")]
        public void GivenIGoToSeleniumFrameworkWebsite()
        {
            home.GoToURL("http://www.seleniumframework.com/Practiceform/");
        }

        [When(@"I click the new browser window button")]
        public void WhenIClickTheNewBrowserWindowButton()
        {
            home.ClickNewBrowserWindow();
        }

        [Then(@"I will see the new browser popup will open")]
        public void ThenIWillSeeTheNewBrowserPopupWillOpen()
        {
            newWindow.ValidateNewWindow();
        }

        [When(@"I go to cucumber tutorial")]
        public void ThenIGoToCucumberTutorial()
        {
            newWindow.GoToCucumber();
        }

        [Then(@"I be directed in Cucumber Tutorial page")]
        public void ThenIBeDirectedInCucumberTutorialPage()
        {
            newWindow.ValidateCucumberJVMPage();
        }

        [When(@"I click the new message browser button")]
        public void WhenIClickTheNewMessageBrowserButton()
        {
            home.ClickNewMessageWindow();
        }

        [Then(@"I will see the new message popup is displayed")]
        public void ThenIWillSeeTheNewMessagePopupIsDisplayed()
        {
            newMessage.ValidateNewMessageWindow();
        }

        [When(@"I click the new browser tab button")]
        public void WhenIClickTheNewBrowserTabButton()
        {
            home.ClickNewBrowserTab();
        }

        [Then(@"I will see the new browser tab is displayed")]
        public void ThenIWillSeeTheNewBrowserTabIsDisplayed()
        {
            newTab.ValidateTabTitle();
        }

        [When(@"I click the alert button")]
        public void WhenIClickTheAlertButton()
        {
            home.ClickJavaScriptAlert();
        }

        [Then(@"I will see the popup message")]
        public void ThenIWillSeeThePopupMessage()
        {
            home.ValidateAlert();
        }

        [Then(@"I can click ok in the alert popup")]
        public void ThenICanClickOkInTheAlertPopup()
        {
            home.ClickAlertConfirmation();
        }

        [When(@"I click the timing alert button")]
        public void WhenIClickTheTimingAlertButton()
        {
            home.ClickTimingAlert();
        }

    }
}
