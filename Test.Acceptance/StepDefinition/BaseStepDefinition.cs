﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TechTalk.SpecFlow;
using Test.Acceptance.Infrastracture;
using Test.Acceptance.Library.Pages;

namespace Test.Acceptance.StepDefinition
{
    class BaseStepDefinition
    {
        protected BasePageModel basePageModel = new BasePageModel();

        protected Home home = new Home();
        protected NewMessage newMessage = new NewMessage();
        protected NewWindow newWindow = new NewWindow();
        protected NewTab newTab = new NewTab();

        [AfterScenario]
        public void DisposeDriver()
        {
            basePageModel.Dispose();
        }
    }
}
