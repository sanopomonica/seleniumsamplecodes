﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace Test.Acceptance.Infrastracture
{
    public static class StaticDriver
    {
        private static IWebDriver _driver;

        public static IWebDriver driver
        {
            get
            {
                if (_driver == null)
                {
                    _driver = new ChromeDriver();
                }
                return _driver;
            }
            private set
            {
                _driver = value;
            }
        }

        public static void CloseDriver()
        {
            _driver.Quit();
            _driver = null;
        }

    }
}
